# Trigger Matching 

Tool to be used to perform the online jet trigger matching to the offline jet with the b-tagging information added as a decorator.

N.B. This tool is meant to be used as a sub-module of the HH4b analysis MNT production framework, XhhCommon, which makes use of xAOD ANA Helpers
